// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator } from "cc";
import Util from "./Util";

class DataManger {
    public static readonly instance = new DataManger();

    /**当前操控人物面向方向(用于展示建筑场景等)*/
    public curHeroDirection: string = "";

    /**
     * 方向类型
     * 属性名对应文件夹名字不可乱改
    */
    public directionType = {
        Back: "BACK",
        Front: "FRONT",
        Left_Side: "LEFT_SIDE",
        Right_Side: "RIGHT_SIDE",
    }

    /**
     * 动作种类对应的节点名
     * 用于自动创建人物帧动画
    */
    public actionCategoryChildNames = {
        base: "move",
        attack: "attack",
        hurt: "hurt"
    }

    /**
     * 动作种类
    */
    public actionCategory = {
        base: { // 基础种类
            Died: "DIED",
            Idle: "IDLE",
            Run: "RUN",
            Walk: "WALK"
        },
        attack: { // 战斗种类
            Attack_1: "ATTACK_1",
            Attack_2: "ATTACK_2",
        },
        hurt: { // 跳跃种类
            Hurt: "HURT"
        }
    }

    /**
     * 生物对应动作类型
     * 属性名对应文件夹名字不可乱改
    */
    public actionType = {
        ANIMAL_BASE: this.actionCategory.base,
        PERSON_SWORDMAN: Util.assign(this.actionCategory.base, this.actionCategory.attack, this.actionCategory.hurt)
    }
}

export default DataManger.instance;
