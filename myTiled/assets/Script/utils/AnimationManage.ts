// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import DataManger from "./DataManger";
import { Component, _decorator, Node, Animation, Sprite, assetManager, SpriteFrame, AnimationClip, AssetManager } from "cc";

const {ccclass, property} = _decorator;


class AnimationManage {
    public static readonly instance = new AnimationManage();

    /**
     * 根据路径给节点添加动画组件
     * @param node 需要添加动画组件的节点
     * @param nodeType 节点类型
     * @param aniFileName 保存动画图片的文件夹名
     * @param customObj 需要自定义的属性
     * {
     *  isLoop,         是否循环播放    默认不循环
     *  call,           播放完成的回调  默认为空
     *  isClearCall,    是否在一次调用后清除回调    默认清除
     *  isLockCall,     是否锁住事件不允许执行(此属性会添加在节点上)    默认不锁
     * }
    */
    public addAniByNode(node: Node, nodeType: string, aniFileName: string, customObj?) {
        let animation = node.getComponent(Animation);
        let sprite = node.getComponent(Sprite);
        let isLoop = (customObj && customObj.isLoop !== undefined) ? customObj.isLoop : false;
        let call = (customObj && customObj.isLoop !== undefined) ? customObj.call : null;
        let isClearCall = (customObj && customObj.isLoop !== undefined) ? customObj.isClearCall : true;
        let isLockCall = (customObj && customObj.isLoop !== undefined) ? customObj.isLockCall : false;

        let WrapMode = isLoop ? AnimationClip.WrapMode.Loop : AnimationClip.WrapMode.Normal;

        !animation && (animation = node.addComponent(Animation));
        !sprite && (sprite = node.addComponent(Sprite));
        
        this.createAni(aniFileName, node, nodeType, WrapMode, call)
    }

    /**
     * 创建动画剪辑
    */
    public createAni(aniFileName: string, node: Node, nodeType: string, WrapMode: AnimationClip.WrapMode, call) {
        let pathObj = this.getAniPathList(nodeType, aniFileName);
        let actionNames = DataManger.actionType[nodeType];
        let abName = "";
        let loadNum = 0;
        let MyAnimation = null; 

        let consloeNum = 0;
        let consloeName = "";
        
        switch(nodeType) {
            case NodeType.animal:
                abName = "person";
                consloeName = "生物->";
                break;
            case NodeType.person.swordman:
                abName = "person";
                consloeName = "人类剑士->";
                break;
        }
        
        console.log("需要加载的帧动画文件夹路径有：", pathObj);
        
        assetManager.loadBundle(abName, (err, ab: AssetManager.Bundle)=> {
            for(let i in pathObj) {
                let directionName = DataManger.directionType[i];
                let direction = pathObj[i];
                
                for(let j in direction) {
                    let actionName = actionNames[j];
                    let dirPath = direction[j];
                    let aniName = nodeType + directionName + actionName;
                    let actionCategory = this.getActionCategoryByActionName(actionName);
                    let aniNode = this.getChildByActionCategory(actionCategory, node);
                    MyAnimation = aniNode.getComponent(Animation);

                    !MyAnimation && aniNode.addComponent(Animation);
                    !aniNode.getComponent(Sprite) && aniNode.addComponent(Sprite);
                    // 加载ab包：异步加载，加载好会回调你的函数;
                        // let pngList = ab.getDirWithPath(dirPath, cc.SpriteFrame) as Array<cc.SpriteFrame>;
                        let spriteFrames = [];
                        // let loadEndNum = 0;
                        loadNum ++;
                        ab.loadDir(dirPath, SpriteFrame, (err, assets) => {
                            spriteFrames = assets;
                            let clip = AnimationClip.createWithSpriteFrames(spriteFrames, spriteFrames.length);//创建一组动画剪辑

                            console.log(consloeName + "动作加载数：", ++consloeNum, "动作名：", aniName);
                            console.log("当前动画创建节点：", aniNode);
                            clip.sample = 10; // 设置动画播放帧率
                            clip.speed = 1; // 设置动画播放速度
                            clip.wrapMode = WrapMode;//设置播放模式
                            clip.name = aniName;//设置动画名字
                            MyAnimation.addClip(clip);//添加动画帧到动画组件中
                            if(--loadNum == 0) {
                                call && call(MyAnimation);
                            }
                        })

                        // 释放ab包, 不会释放从ab包里面加载好的资源;
                        // assetManager.removeBundle(ab);
                }

            }
        });
    }

    /**
     * 根据需要文件夹名字及类型获取需要的资源路径
    */
    private getAniPathList(nodeType, aniFileName) {
        let pathObj = {};
        let basePath = "";
        let actionNames = [];

        switch (nodeType) {
            case NodeType.person.swordman:
                console.log("开始获取人类剑士需要的帧动画路径");
                basePath = "swordman/" + aniFileName + "/";
                actionNames = DataManger.actionType[NodeType.person.swordman];
                break;
            case NodeType.animal:
                console.log("开始获取生物需要的帧动画路径");
                basePath = "swordman/" + aniFileName + "/";
                actionNames = DataManger.actionType[NodeType.animal];
                break;
        }

        for(let i in DataManger.directionType) {
            let path = basePath + i + "/";
            pathObj[i] = {};
            for(let j in actionNames) {
                pathObj[i][j] = path + j;
            }
        }

        return pathObj;
    }

    /**
     * 根据动作名获取动作种类
    */
    private getActionCategoryByActionName(actionName) {
        let actionCategory = "";
        for(let i in DataManger.actionCategory) {
            let curCategorys = DataManger.actionCategory[i];
            for(let j in curCategorys) {
                if(actionName == curCategorys[j]) {
                    actionCategory = i;
                    return actionCategory;
                }
            }
        }
    }

    /**
     * 根据动作种类获取对应节点
    */
    private getChildByActionCategory(actionCategory, node) {
        let nodeName = "";

        nodeName = DataManger.actionCategoryChildNames[actionCategory];

        if(nodeName !== "") {
            return node.getChildByName(nodeName);
        } else {
            console.warn("当前动作类型对应节点未找到");
            return null;
        }
    }
}

/**
 * 动画类型 
*/
export class NodeType {
    static animal = "ANIMAL_BASE"
    static person = {
        swordman: "PERSON_SWORDMAN",
    }
}



export default AnimationManage.instance;
