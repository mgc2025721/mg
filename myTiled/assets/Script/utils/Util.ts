// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import { _decorator } from "cc";

class Util {
    public static readonly instance = new Util();

    /**
     * 返回多个对象拼接后对象
     * 若有相同属性则会被最后传入的对象属性覆盖
    */
    assign (...sources: any[]): any {
        let target = {};
        
        for(let i: number = 0; i < sources.length; i++) {
            let obj = sources[i];
            for(let j in obj) {
                target[j] = this.deepClone(obj[j]);
            }
        }

        return target;
    }

    /**
     * 深拷贝
    */
    deepClone(obj) {
        let newObj = {};
        for(let i in obj) {
            if(obj[i] == {}) {
                newObj[i] = this.deepClone(obj[i]);
            } else {
                newObj[i] = obj[i];
            }
        }
        return newObj;
    }
}

export default Util.instance;
