import AnimationManage, { NodeType } from "./utils/AnimationManage";

import { Component, _decorator, Node, TiledLayer, Size, Animation } from "cc";
import DataManger from "./utils/DataManger";
import Util from "./utils/Util";
import { AnimalBase } from "./animal/AnimalBase";

const {ccclass, property} = _decorator;

@ccclass
export default class Main extends Component {

    @property({type: Node, tooltip: "地图节点"})
    mapNode: Node = null;

    @property({type: Node, tooltip: "人物节点"})
    heroNode: Node = null;

    start () {
        // init logic
        this.initMap(); 
    }

    initMap() {
        //根据图层名获取图层
        let layer:TiledLayer = this.mapNode.getChildByName("Tile Layer 1").getComponent(TiledLayer);
        //获取图层的行列数
        let layerSize:Size = layer.getLayerSize();
        let width = layerSize.width;
        let height = layerSize.height;
        console.log(layerSize);  // size(width:40, height:30)
        //获取图层的gid
        let mapData = [];
        for(let i=0;i<height;i++){
            mapData[i] = [];
            for(let j=0;j<width;j++){
                mapData[i].push(layer.getTileGIDAt(j,i));
            }
        }

        AnimationManage.addAniByNode(this.heroNode, NodeType.animal, "1", {isLoop: true, call: (animation) => {
            this.heroNode.getComponent(AnimalBase).MyAnimation = this.heroNode.getChildByName(DataManger.actionCategoryChildNames.base).getComponent(Animation);
            this.heroNode.getComponent(AnimalBase).nodeAniLoadEnd = true;
            let aniName = NodeType.person.swordman + DataManger.directionType.Back + DataManger.actionType.PERSON_SWORDMAN.Idle;
            animation.play(aniName);
        }})
    }
}
