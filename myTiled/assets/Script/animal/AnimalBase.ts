import { _decorator, Component, Node, input, Input, KeyCode, Scheduler, Vec2, v2, v3, Animation } from 'cc';
import { NodeType } from '../utils/AnimationManage';
import DataManger from "../utils/DataManger";
const { ccclass, property } = _decorator;

@ccclass('AnimalBase')
export class AnimalBase extends Component {

    @property({type: Node, tooltip: "血条节点"})
    bloodBarNode: Node = null;

    @property({type: Node, tooltip: "移动动画播放节点"})
    animalMoveAniNode: Node = null;

    /**当前人物展示的资源方向*/
    protected _curDirection: string = "";

    protected get curDirection () {
        return this._curDirection;
    }
    protected set curDirection (direction) {
        this.lastDirection = this._curDirection;
        this._curDirection = direction;
    }

    readonly a: string = "";
    /**上次人物展示的资源方向*/
    protected lastDirection: string = "";
    /**当前播放的移动动画名*/
    protected curPlayAnimalAniName: string = "";
    /**节点动画已经加载完毕*/
    public nodeAniLoadEnd: boolean = false;
    /**当前节点类型*/
    protected nodeType:string = NodeType.animal;
    /**动画对象*/
    public MyAnimation: Animation = null;
    /**当前正在播放的动画*/
    protected curPlayAniList = [];
    /**键盘连续按下事件触发间隔*/
    protected keyboardInterval: number = 0.1;
    /**键盘按住的按键keyCode集合*/
    protected keyPressingKeyCodeList: number[] = [];
    /**每次移动的距离(像素)*/
    protected moveSpeed: number = 2;

    start() {     

        input.on(Input.EventType.KEY_DOWN, this.keyboardDown, this);
        input.on(Input.EventType.KEY_UP, this.keyboardUp, this);
    }

    /**
     * 注册键盘长按事件
    */
    private registerMyKeyPressing(keyCode) {
        let pressingSchedule = new keyPressingSchedule();
        let self = this;

        this.schedule(this.keyPressingFunc, this.keyboardInterval);
        if(this.ieMoveKeyDown(keyCode)) {
            this.keyPressingKeyCodeList.push(keyCode);
        }
    }

    /**
     * 从数组内移除键盘长按的按键
    */
    private removeMyKeyPressing(keyCode) {
        this.keyPressingKeyCodeList = this.keyPressingKeyCodeList.filter((code) => {
            return code != keyCode;
        });
    }

    
    /**键盘有按键按住时运行的方法*/
    private keyPressingFunc() {
        this.keyPressingAnimalMove();
    }

    /**
     * 键盘按下事件
    */
    keyboardDown(event) {
        this.runActionByKeyCod(event.keyCode);
        this.registerMyKeyPressing(event.keyCode);
    }

    /**
     * 键盘弹起
    */
    keyboardUp(event) {
        console.log("键盘弹起了");
        this.removeMyKeyPressing(event.keyCode);
        if(this.keyPressingKeyCodeList.length == 0) {
            this.unschedule(this.keyPressingFunc);
            let aniName = this.nodeType + this.curDirection + DataManger.actionType.PERSON_SWORDMAN.Idle;
            
            if(this.nodeAniLoadEnd && this.curPlayAnimalAniName !== aniName) {
                this.curPlayAnimalAniName = aniName;
                this.MyAnimation.play(aniName);
            } 
        }
    }

    /**
     * 根据keyCode执行相应动作
    */
    runActionByKeyCod(keyCod) {
        let consloeStr = "按下了按键-";
        let movePos = null;

        switch(keyCod) {
            case KeyCode.KEY_W:
                consloeStr += "W";
                this.curDirection = DataManger.directionType.Back;
                movePos = v2(0, this.moveSpeed);
                break;
            case KeyCode.ARROW_UP:
                consloeStr += "上";
                this.curDirection = DataManger.directionType.Back;
                movePos = v2(0, this.moveSpeed);
                break;
            case KeyCode.KEY_S:
                consloeStr += "S";
                this.curDirection = DataManger.directionType.Front;
                movePos = v2(0, -this.moveSpeed);
                break;
            case KeyCode.ARROW_DOWN:
                consloeStr += "下";
                this.curDirection = DataManger.directionType.Front;
                movePos = v2(0, -this.moveSpeed);
                break;
            case KeyCode.KEY_A:
                consloeStr += "A";
                this.curDirection = DataManger.directionType.Left_Side;
                movePos = v2(-this.moveSpeed, 0);
                break;
            case KeyCode.ARROW_LEFT:
                consloeStr += "左";
                this.curDirection = DataManger.directionType.Left_Side;
                movePos = v2(-this.moveSpeed, 0);
                break;
            case KeyCode.KEY_D:
                consloeStr += "D";
                this.curDirection = DataManger.directionType.Right_Side;
                movePos = v2(this.moveSpeed, 0);
                break;
            case KeyCode.ARROW_RIGHT:
                consloeStr += "右";
                this.curDirection = DataManger.directionType.Right_Side;
                movePos = v2(this.moveSpeed, 0);
                break;
        }


        console.log(consloeStr);
        this.animalMove(movePos);
    }

    /**
     * 键盘按住时移动检测
    */
    private keyPressingAnimalMove() {
        let topIsDown: boolean = false;
        let bottomIsDown: boolean = false;
        let leftIsDown: boolean = false;
        let rightIsDown: boolean = false;
        let movePos = v2(0, 0);
        let moveObj = {
            top: v2(0, this.moveSpeed),
            bottom: v2(0, -this.moveSpeed),
            left: v2(-this.moveSpeed, 0),
            right: v2(this.moveSpeed, 0),
        };

        for(let i: number = 0; i < this.keyPressingKeyCodeList.length; i++) { // 获取那些移动相关按钮处于按下状态
            switch(this.keyPressingKeyCodeList[i]) {
                case KeyCode.KEY_W:
                case KeyCode.ARROW_UP:
                    if(!topIsDown) {
                        movePos = moveObj.top;
                        this.curDirection = DataManger.directionType.Back;
                        // movePos = v2(movePos.x + moveObj.top.x, movePos.y + moveObj.top.y);
                    }
                    topIsDown = true;
                    break;
                case KeyCode.KEY_S:
                case KeyCode.ARROW_DOWN:
                    if(!bottomIsDown) {
                        movePos = moveObj.bottom;
                        this.curDirection = DataManger.directionType.Front;
                        // movePos = v2(movePos.x + moveObj.bottom.x, movePos.y + moveObj.bottom.y);
                    }
                    bottomIsDown = true;
                    break;
                case KeyCode.KEY_A:
                case KeyCode.ARROW_LEFT:
                    if(!leftIsDown) {
                        movePos = moveObj.left;
                        this.curDirection = DataManger.directionType.Left_Side;
                        // movePos = v2(movePos.x + moveObj.left.x, movePos.y + moveObj.left.y);
                    }   
                    leftIsDown = true;                                            
                    break;
                case KeyCode.KEY_D:
                case KeyCode.ARROW_RIGHT:
                    if(!rightIsDown) {
                        movePos = moveObj.right;
                        this.curDirection = DataManger.directionType.Right_Side;
                        // movePos = v2(movePos.x + moveObj.right.x, movePos.y + moveObj.right.y);
                    }
                    rightIsDown = true;
                    break;  
            }
        }
        this.animalMove(movePos);
    }

    /**
     * 是否为移动按键
    */
    private ieMoveKeyDown(keyCode) { 
        let isMove = false;
        switch(keyCode) {
            case KeyCode.KEY_W:
            case KeyCode.ARROW_UP:
            case KeyCode.KEY_S:
            case KeyCode.ARROW_DOWN:
            case KeyCode.KEY_A:
            case KeyCode.ARROW_LEFT:
            case KeyCode.KEY_D:
            case KeyCode.ARROW_RIGHT:
                isMove = true;
        }
        return isMove;
    }

    /**
     * 动物移动
    */
    animalMove(movePos: Vec2) {
        let basePos = this.node.position;
        let aniName = this.nodeType + this.curDirection + DataManger.actionType.PERSON_SWORDMAN.Walk;
        
        // 防止未加载完成动画时动画播放异常与同一方向行动时动画一直重播不连贯
        if(this.nodeAniLoadEnd && this.curPlayAnimalAniName !== aniName) {
            this.curPlayAnimalAniName = aniName;
            this.MyAnimation.play(aniName);
        } 
        this.node.position = v3(basePos.x + movePos.x, basePos.y + movePos.y);
    }

    /**
     * 判断是否需要中断
    */
}

class keyPressingSchedule {
    key: number = 0
    scheduleFunc: Function = null;
}


